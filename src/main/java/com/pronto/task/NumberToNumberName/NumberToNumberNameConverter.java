package com.pronto.task.NumberToNumberName;

//com.pronto.task.NumberToNumberName.NumberToNumberNameConverter;

import java.util.Scanner;

public class NumberToNumberNameConverter {

    private static final String[] ones = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
    private static final String[] teens = {"", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    private static final String[] tens = {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};

    public static String numToNumName(int number) {
        boolean isThousands = false;
        if (number < 0 || number > 999999) {
            return "Number out of range";
        }

        if (number == 0) {
            return ones[0];
        }

        StringBuilder result = new StringBuilder();
        int thousandsIndex = 0;

        while (number > 0) {
            if (number % 1000 > 0) {
                if (result.length() > 0) {
                    result.insert(0, ", ");
                }
                result.insert(0, convertNumberToNumberName(number % 1000) + " " + ((isThousands) ? "Thousand" : ""));
            }
            number /= 1000;
            isThousands=true;
        }

        return result.toString();
    }

    private static String convertNumberToNumberName(int number) {
        if (number < 10) {
            return ones[number];
        }
        else if (number == 10) {
            return tens[1];
        }
        else if (number < 20) {
            return teens[number - 10];
        }
        else if (number < 100) {
            return tens[number / 10] + ((number % 10 != 0) ? "-" + ones[number % 10] : "");
        }
        else if (number < 1000) {
            return ones[number / 100] + " Hundred" + ((number % 100 != 0) ? " and " + convertNumberToNumberName(number % 100) : "");
        }
        else {
            return convertNumberToNumberName(number / 1000) + "Thousand" + ((number % 1000 != 0) ? ", " + convertNumberToNumberName(number % 1000) : "");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter an integer between 0 and 999999 ( N to exit ): ");

        while (true) {

            if (!scanner.hasNextInt()) {
                String input = scanner.next();
                System.out.print("Exiting ");
                if (input.equals("N")) {
                    System.exit(0);
                }
                break;
            }

            int number = scanner.nextInt();
            String textRepresentation = numToNumName(number);
            System.out.println(textRepresentation);
            System.out.print("Enter an integer between 0 and 999999 ( N to exit ): ");
        }
        scanner.close();
    }
}
