package com.pronto.task.NumberToNumberName;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.Scanner;

@SpringBootApplication
public class NumberToNumberNameApplication  implements CommandLineRunner{

	@Override
	public void run(String... args) {
		System.out.println("Forking process");
		new Thread(() -> {
			System.out.println("In child process");
			String mainClass = "com.pronto.task.NumberToNumberName.NumberToNumberNameConverter";
			String[] command = {"java", "-cp", "target/my-app-0.0.1-SNAPSHOT.jar", mainClass};
			ProcessBuilder processBuilder = new ProcessBuilder(command);
			try {
				processBuilder.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
	}

}
