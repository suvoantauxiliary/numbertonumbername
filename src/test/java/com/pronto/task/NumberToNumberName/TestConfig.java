package com.pronto.task.NumberToNumberName;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Bean
    public NumberToNumberNameConverter numberToNumberNameConverter() {
        return new NumberToNumberNameConverter();
    }
}
