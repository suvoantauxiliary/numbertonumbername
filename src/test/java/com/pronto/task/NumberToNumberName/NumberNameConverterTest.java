package com.pronto.task.NumberToNumberName;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = TestConfig.class)
public class NumberNameConverterTest {

    @Autowired
    private NumberToNumberNameConverter numberToTextConverter;

    @Test
    @DirtiesContext
    public void testIntToText() {
        assertEquals("Zero", numberToTextConverter.numToNumName(0).trim());
        assertEquals("One", numberToTextConverter.numToNumName(1).trim());
        assertEquals("Ten", numberToTextConverter.numToNumName(10).trim());
        assertEquals("Fifty-Six", numberToTextConverter.numToNumName(56).trim());
        assertEquals("Two Hundred and Twelve", numberToTextConverter.numToNumName(212).trim());
        assertEquals("One Thousand, Two Hundred and Thirty-Four", numberToTextConverter.numToNumName(1234).trim());
        assertEquals("Two Hundred and Twenty-Two Thousand, Two Hundred and Twenty-Two", numberToTextConverter.numToNumName(222222).trim());
        assertEquals("Number out of range", numberToTextConverter.numToNumName(1000000));
    }
}

