# Number to Number Name Converter

This application will take upto 6 digit numbers as input from user via the console and convert them to its number name format.

The Application is invoked via cli by following steps
* Checkout the application and go to the context root [/NumberToNumberName]
and run command - **mvnw.cmd clean install** <br> ![img.png](images/img.png)
* Now run the jar file to invoke the application by running below command <br> 
**java -jar ./target/NumberToNumberName-0.0.1-SNAPSHOT.jar com.pronto.task.NumberToNumberName.NumberToNumberNameConverter**

* As soon as you run above command the application will start and promt to input numbers
![img.png](images/img2.png)

* Continue providing numbers and get the corresponding number names. To exit press N 
![img_1.png](images/img_1.png)

<hr>
Important Classes
<hr>
* NumberToNumberNameConverter - Contains logic to convert. <br>
* NumberNameConverterTest - Contains tests 